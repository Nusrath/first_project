
def read_tsv(filename):

    up_measures = []
    reports = []
    
    with open(filename, 'r', encoding='utf-8') as file:
        for line in file:
            upm, _, _, _, rep = line.strip().rsplit('\t', maxsplit=4)

            up_measures.append(upm)
            reports.append(rep)
    
    unique_lbl = set(up_measures)

    for lbl in unique_lbl:
        print(lbl)


if __name__ == '__main__':

    read_tsv('1200_coded_reports.txt')